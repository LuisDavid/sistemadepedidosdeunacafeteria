function Orden(){
  this.idUsuario;
  this.total=0.0;
  this.productos=new Array();
  this.ubicacion;
  this.getNombre = function(){
    return this.nombre;
  }

  this.getIdUsuario = function(){
    return this.idUsuario;
  }
  
  this.getProductos = function(){
    return this.productos;
  }
  this.getTotal = function(){
    return this.total;
  }
  this.getUbicacion = function(){
    return this.ubicacion;
  }
 
  this.setNombre = function(n){
    this.nombre=n;
  }
 
  this.setIdUsuario = function(idUsuario){
    this.idUsuario=idUsuario;
  }

  this.setProductos= function(productos){
    this.productos=productos;
  }
  this.setTotal = function(total){
    this.total=total;
  }
  this.agregarProducto=function(id){
    this.productos.push({'idProducto':id,'idCombo': null})
  }
  this.setUbicacion=function(u){
    this.ubicacion=u
  }
}
function Producto(){
  this.id;
  this.nombre;
  this.descripcion;
  this.precio=0.0;
  this.tipo;
  this.imagen;
  this.getId =function(){
    return this.id
  }
  this.getNombre =function(){
    return this.nombre
  }
  this.getDescripcion =function(){
    return this.descripcion
  }
  this.getPrecio =function(){
    return this.precio
  }
  this.getTipo =function(){
    return this.tipo
  }
  this.getImagen =function(){
    return this.imagen
  }
  this.setId =function(dato){
    this.id=dato
  }
  this.setNombre =function(dato){
    this.nombre=dato
  }
  this.setDescripcion =function(dato){
    this.descripcion=dato
  }
  this.setPrecio =function(dato){
    this.precio=dato
  }
  this.setTipo =function(dato){
    this.tipo=dato
  }
  this.setImagen =function(dato){
    this.imagen=dato
  }
}