import psycopg2
import os
from hashlib import sha256
from time import strftime
class Usuario:
	def __init__(self):
		self.correo=""
		self.password=""
		self.activo=False
		self.clave=""
	def setPassword(self,password):
		self.password=password
	def setCorreo(self,correo):
		self.correo=correo
	def setActivo(self,activo):
		self.activo=activo
	def setClave(self,clave):
		self.clave=clave
	def getPassword(self):
		return self.password
	def getCorreo(self):
		return self.correo
	def getActivo(self):
		return self.activo
	def getclave(self):
		return self.clave
		
def getConexion():
	conn = psycopg2.connect(database='cafeteria',user='postgres',password='p057gr35', host='localhost')
	return conn;

def getUsuarios():
	conn = getConexion()
	cur = conn.cursor()
	#cur.execute("insert into artes_marciales (nombre,origen,significado,objetivo) values (%s,%s,%s,%s);" ,fila)
	cur.execute("select correo,pass from usuario where activo=true")
	usuarios={}
	while True:
		usuario=cur.fetchone()	
		if(usuario==None):
			break
		usuarios[usuario[0]]=usuario[1]
	#conn.commit()
	conn.close()
	return usuarios

def existeUsuario(correo):
	conn = getConexion()
	cur = conn.cursor()
	cur.execute("select	activo from usuario where correo = %s",[correo])
	usuario=cur.fetchone()
	if(usuario==None):
		return False
	return True

def guardarUsuario(usuario):
	conn = getConexion()
	cur=conn.cursor();
	password_hash= sha256(usuario.getPassword().encode()).hexdigest()
	cur.execute("insert into usuario (correo,pass,activo,clave) values (%s,%s,%s,%s);",
		[usuario.getCorreo()
		,password_hash
		,usuario.getActivo()
		,usuario.getclave()])
	conn.commit()
	conn.close()

def validarRegistro(clave):
	conn = getConexion()
	cur = conn.cursor()
	cur.execute("select	correo from usuario where clave = %s",[clave])
	usuario=cur.fetchone()
	if(usuario==None):
		return "No existe registro para validar"
	print(usuario[0])
	cur.execute("update usuario set activo = true where correo = %s",[usuario[0]])
	conn.commit()
	conn.close()
	return "Confirmacion exitosa "+usuario[0]

def guardarProducto(producto):
	if producto is not None:
		conn = getConexion()
		cur=conn.cursor();
		url='http://10.42.0.1/images/'+str(producto['nombreImagen'])
		cur.execute("insert into producto (nombre,descripcion,tipo,precio,disponible,imagen_url) values (%s,%s,%s,%s,%s,%s);",
		[producto['nombre']
		,producto['descripcion']
		,producto['tipo']
		,float(producto['precio'])
		,True
		,url])
		print(" guardar producto")
		conn.commit()
		conn.close()
		return True
	return False

def obtenerProductos():
	conn = getConexion()
	cur = conn.cursor()
	cur.execute("select	id,nombre,tipo,precio,imagen_url,descripcion from producto where disponible= true")
	productos=[]
	while True:
		producto=cur.fetchone()	
		if(producto==None):
			break
		productos.append({'id':producto[0],'nombre':producto[1]
				,'tipo':producto[2],'precio':producto[3]
				,'url':producto[4]
				,'descripcion':producto[5]})
	conn.close()
	return productos
def guardarCombo(combo):
	if combo is not None:
		conn = getConexion()
		cur=conn.cursor();
		cur.execute("insert into combo (nombre,descripcion,precio) values (%s,%s,%s);",
		[combo['nombre']
		,combo['descripcion']
		,float(combo['precio'])])
		#Se obtiene el ultimo numero serial que se asigno
		cur.execute("select last_value from combo_id_seq")
		idCombo=cur.fetchone()[0];

		for idProducto in combo['idProductos']:
			cur.execute("insert into combo_producto (fk_combo,fk_comida) values(%s,%s)"
				,[idCombo,idProducto])
		conn.commit()
		conn.close()
		return True
	return False

def obtenerCombos():
	conn = getConexion()
	cur = conn.cursor()
	cur.execute("select	id,nombre,precio,descripcion from combo")
	combos=[]
	while True:
		res=cur.fetchone()	
		if(res==None):
			break
		cur2 = conn.cursor()
		combo={'id':res[0],'nombre':res[1]
				,'precio':res[2]
				,'descripcion':res[3]}

		cur2.execute("select FK_comida from combo_producto where FK_combo = %s",[combo['id']])
		productos=[]
		cur3=conn.cursor();
		while True:
			cur3.execute("select nombre,descripcion,tipo,imagen_url from producto where id= %s",[cur2.fetchone()])
			producto=cur3.fetchone()
			if(producto is None):
				break
			productos.append({'nombre':producto[0]
				,'descripcion':producto[1]
				,'tipo':producto[2]
				,'url':producto[3]})
		combo['productos']=productos
		combos.append(combo)
	conn.close()
	return combos

#
#n : orden no pagada
#p : orden pagada
def agregarOrden(orden):
	if orden is not None:
		conn = getConexion()
		cur = conn.cursor()
		fecha=strftime("%c")
		hora=strftime("%X") # tipo de formato %c fecha y hora actual 
		cur.execute("insert into orden_usuario (Fk_usuario,fecha,hora,ubicacion,total,estado) values\
			(%s,%s,%s,%s,%s,%s)",[orden['idUsuario'],fecha,hora,orden['ubicacion'],orden['total'],'n']) 
		pedidos=orden['productos']
		conn.commit()
		cur.execute("select last_value from orden_usuario_id_seq");
		idOrden=cur.fetchone()[0];

		print("id de la orden: "+str(idOrden))
		for pedido in pedidos:
			cur.execute("insert into pedido (fk_usuario,fk_orden,idCombo,idProducto) values\
				(%s,%s,%s,%s)",[orden['idUsuario'],idOrden,pedido['idCombo'],pedido['idProducto']])
			conn.commit()
		print("Se agrego exitosamente el pedido")
		return True
	return False
def obtenerNumPedidos():
	conn = getConexion()
	cur = conn.cursor()
	cur.execute("select last_value from orden_usuario_id_seq");
	num=cur.fetchone()[0];
	return num
def obtenerPedidosBD():
	conn = getConexion()
	cur = conn.cursor()
	fecha=strftime("%c")
	cur.execute("select	id,FK_usuario,ubicacion,total,estado,hora from orden_usuario where fecha=%s\
				 order by hora DESC",[fecha])
	pedidos=[]
	while True:
		p=cur.fetchone()
		if p is None:
			break
		pedido={'idOrden':p[0],
						'idUsuario':p[1],
						'ubicacion':p[2],
						'total':p[3],
						'estado': p[4],
						'hora': str(p[5])}
		cur2=conn.cursor()
		cur2.execute("select idCombo,idProducto from pedido where fk_orden=%s",[p[0]])
		productos=[]
		while True:
			producto=cur2.fetchone()
			if producto is None:
				break;
			cur3=conn.cursor()
			if producto[0] is not None:
				cur3.execute("select nombre,precio,descripcion from combo where id=%s",[producto[0]])
				c=cur3.fetchone()
				combo={'nombre':c[0],'precio':c[1],'descripcion':c[2]}
				productosCombo=[]
				cur4=conn.cursor()
				cur5=conn.cursor()
				cur4.execute("select fk_comida from combo_producto where fk_combo=%s",[producto[0]])
				while True:
					idProducto=cur4.fetchone()
					if idProducto is None:
						break
					p=cur5.execute("select nombre,descripcion,tipo,disponible,imagen_url from\
								 producto where id=",[idProducto[0]])
					productosCombo.append({'nombre':p[0],'descripcion':p[1],'tipo':p[2],
									  'disponible':p[3],'imagen_url':p[4]})
				combo['productos']=productosCombo
				productos.append(productosCombo)

			if producto[1] is not None:
				cur3.execute("select nombre,descripcion,tipo,disponible,imagen_url from\
								 producto where id=%s",[producto[1]])
				print("producto: " +str(producto[1]))
				p=cur3.fetchone()
				productos.append({'nombre':p[0],'descripcion':p[1],'tipo':p[2],
									  'disponible':p[3],'imagen_url':p[4] })
			pedido['productos']=productos
		pedidos.append(pedido)
	return pedidos




if __name__ == '__main__':
	usuario = Usuario()
	usuario.setCorreo("luis@sada.c")
	usuario.setPassword("1234")
	usuario.setClave("asdadsadds")
	usuario.setActivo(True)
	#guardarUsuario(usuario)
	print("existe: ",existeUsuario("admin"))
	print(os.urandom(8))
	combos=obtenerCombos();
	for combo in combos:
		print(combo['id'])
		print(combo['nombre'])
		print(combo['descripcion'])
		print(combo['productos'])

	agregarOrden({'idUsuario':'admin','ubicacion':'E-212','pedidos':[{'idProducto':1,'idCombo':None}]})
	pedidos=obtenerPedidosBD();
	print("Super *********************************")
	print(pedidos)
	#Prueba de la funcion validarRegistro
	#validarRegistro("CJGGtfEmFWBENQ4Q9CdKgg==");