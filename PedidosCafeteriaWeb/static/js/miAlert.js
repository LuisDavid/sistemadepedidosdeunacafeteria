
	function CustomAlert(){
	    this.render = function(mensaje,nombreFuncion,id,modo){
	    	var idProducto=id;
	        var winW = window.innerWidth;
	        var winH = window.innerHeight;
	        var dialogoverlay = document.getElementById('dialogoverlay');
	        var dialogbox = document.getElementById('dialogbox');
	        dialogoverlay.style.display = "block";
	        dialogoverlay.style.height = winH+"px";
	        dialogbox.style.left = (winW/2) - (550 * .5)+"px";
	        dialogbox.style.top = "100px";
	        dialogbox.style.display = "block";
	        document.getElementById('dialogboxhead').innerHTML = "Mensaje";
	        document.getElementById('dialogboxbody').innerHTML = mensaje;
	        $('#dialogboxfoot').empty();
	        if(modo=='normal'){
	        	$('#dialogboxfoot').append('<button onclick="Alert.ok()">Ok</button>');
	        }
	        else{
		        $('#dialogboxfoot').append('<button onclick="'+nombreFuncion+'('+id+')">Si</button>');
		        $('#dialogboxfoot').append('<button onclick="Alert.ok()">No</button>');
	        }
	    }
		this.ok = function(){
			document.getElementById('dialogbox').style.display = "none";
			document.getElementById('dialogoverlay').style.display = "none";
		}
	}

	var Alert = new CustomAlert();
