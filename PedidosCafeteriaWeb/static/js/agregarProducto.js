$(document).ready(function(){
	function handleFileSelect(evt) {
		var files = evt.target.files; // FileList object
		f = files[0];
		var reader = new FileReader();
		  // Closure to capture the file information.
		  reader.onload = (function(theFile) {
		    return function(e) {
		      // Render thumbnail.
		      var span = document.createElement('span');
		      span.innerHTML = ['<img class="thumb" src="', e.target.result,
		                        '" title="', escape(theFile.name), '" height="20%" width="20%" id="imgProducto"/>'].join('');
		      console.log("El resultadoes: "+e.target.result);
		      var imagenselec=e.target.result;
		      $("#list").empty();
		      document.getElementById('list').insertBefore(span, null);
		      $('#nuevoProducto').submit(function(evento) {
					$.ajax({
						url: 'agregarProducto',
						type: 'POST',
						data: JSON.stringify({nombre: $('#nombre').val(),
									descripcion: $('#descripcion').val(),
									tipo: $('#postre').val(),
									precio: $('#precio').val(),
									nombreImagen: $('#imagen')[0].files[0].name,
									imagenCodificada: imagenselec}),
						processData: false,
						contentType: "application/json",
						success: function(mensaje){
							//Alert.render("Producto agregado exitosamente","",-1,"normal");
							alert("Producto agregado exitosamente");
							location.replace("agregarProducto")
							console.log("El mensaje fue: "+mensaje);
							//alert("Se agrego exitosamente el producto.");
						},
						error: function(m){
							console.log("Ocurrio un error");
							console.log(m)
							alert("Ocurrio algún error que impidio agregar el producto.","",-1,"normal");
						}
						
					})
		evento.preventDefault();
		})
		      
		    };
		  })(f);
		
		  // Read in the image file as a data URL.
		      reader.readAsDataURL(f);
		  }
	document.getElementById('imagen').addEventListener('change', handleFileSelect, false);

	//para las transiciones
	$.mobile.defaultPageTransition='flip'; 
	$.mobile.defaultDialogTransition='slideup'; 
	
	function clearMensaje(n){
		$('#mensaje'+n).fadeOut();
	}
	
	validarFormulario("agregar",true);
	
	$('#nombre').focus(function(){clearMensaje(1)});
	$('#descripcion').focus(function(){clearMensaje(2)});
	$('#precio').focus(function(){clearMensaje(3)});
	$('#imagen').focus(function(){clearMensaje(4)});
	
	
});
		