$(document).ready(function(){
		$('#nuevoCombo').submit(function(evento) { 
			productosSeleccionados=new Array()
			$("input:checkbox:checked").each(function() {
             	productosSeleccionados.push($(this).val())
        	});
			$.ajax({
				url: 'agregarCombo',
				type: 'POST',
				data: JSON.stringify({nombre: $('#nombre').val(),
							descripcion: $('#descripcion').val(),
							precio: $('#precio').val(),
							idProductos: productosSeleccionados
							}),
				processData: false,
				contentType: "application/json",
				success: function(mensaje){
					//Alert.render("Producto agregado exitosamente","",-1,"normal");
					alert("Producto agregado exitosamente")
					location.replace("agregarCombo")
					console.log("El mensaje recibido fue : "+mensaje);
					//alert("Se agrego exitosamente el producto.");
				},
				error: function(m){
					console.log("Ocurrio un error"+m);
					alert("Ocurrio algún error que impidio agregar el producto.","",-1,"normal");
				}
				
			}); 
			evento.preventDefault();
		})

	function clearMensaje(n){
		$('#mensaje'+n).fadeOut();
	}
	
	validarFormulario("agregar",true);
	
	$('#nombre').focus(function(){clearMensaje(1)});
	$('#descripcion').focus(function(){clearMensaje(2)});
	$('#precio').focus(function(){clearMensaje(3)});
	
	
}
);
	