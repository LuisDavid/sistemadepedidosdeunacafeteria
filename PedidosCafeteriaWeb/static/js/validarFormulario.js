
	var nombreValido=/^[a-zA-Z]/;
	var numeros=/^[0-9]*(.|[0-9])[0-9]*[0-9]$/;
	function validarFormulario(nombreBoton,validarImagen){
		$("#"+nombreBoton).click(function(){
			var nombre=$("#nombre").val();
			var descripcion=$("#descripcion").val();
			var piezas=$("#precio").val();
			var imagen=$("#imagen").val();
			if(nombre=="" || !nombreValido.test(nombre)){
				$("#mensaje1").fadeIn();
				return false;
			}
			else{
				$('#mensaje1').fadeOut();
				if(descripcion==""){
					$("#mensaje2").fadeIn();
					return false;
				}
				else{
					$('#mensaje2').fadeOut();
					if(piezas=="" || !numeros.test(piezas)){
						$("#mensaje3").fadeIn();
						return false;
					}
					else{
						$('#mensaje3').fadeOut();
						if(imagen=="" && validarImagen){
							$("#mensaje4").fadeIn();
							return false;
						}
						else{
							$('#mensaje4').fadeOut();
							return true;
						}
					}
				}
			}
			
		});
	}
