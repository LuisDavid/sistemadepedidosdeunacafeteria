#!/usr/bin/python3
from flask import Flask,render_template,Response,send_from_directory,request,make_response,session,redirect,url_for
from random import randint
from lxml import etree
import uuid #para el uso de random
import jinja2
#from flask_bootstrap import Bootstrap
from flask_cors import cross_origin
from ClientesBD import * #En el servidor para importa este modulo es necesario poner cageteria.ClientesBD
import smtplib 
from flask_mail import Mail,Message
import base64
import json
def validarUsuario(nombre):
	usuarios=getUsuarios()
	try:	
		usuario=usuarios[nombre]
	except:
		usuario=None

	return usuario


app = Flask(__name__)
#Bootstrap(app)
app.secret_key = os.urandom(16)
mail = Mail(app)
hayNuevoPedido=False

@app.route('/')
def inicio():
	#return render_template("administrador.html")
	try:
		if session["activo"]==True:
			return render_template("administrador.html")
	except:
		return render_template('index.html')	
	return render_template('index.html')

@app.route('/hayPedidoReciente',methods=["POST"])
def hayPedidosRecientes():
	if 'activo' in session:
		numPedidosActual=obtenerNumPedidos()
		if session['numPedidos'] < numPedidosActual:
			session['numPedidos']=numPedidosActual
			print("Si hay nuevo pedido")
			return "Si"	
		else:
			print("NO hay pedidos recientes")
			return "No"

	return render_template("index.html")
	
@app.route('/administrador',methods=["POST"])
def administrador():
	usu=validarUsuario(request.form['nombre'])
	if usu is not None 	and usu== request.form['pass']:
		session['activo']=True
		session['numPedidos']=obtenerNumPedidos()
		#resp = make_response(render_template("administrador.html"))
		#resp.set_cookie('admin', 'luis david')
		return redirect('/obtenerPedidos')#render_template("administrador.html",pedidos=pedidosBD)
	return render_template("index.html")#redirect("/")

@app.route('/agregarProducto',methods=['GET','POST'])
def agregarProducto():
	if 'activo' in session:
		if session['activo']:
			data=request.get_json()
			if data != None:
				print("nombre: "+data['nombre'])
				print("tipo: "+data['tipo'])
				print("nombreImagen: "+data['nombreImagen'])
				print("nombreImagen: "+data['imagenCodificada'].split(",")[1])

				image_64_decode = base64.b64decode(data['imagenCodificada'].split(",")[1]) 
				image_result = open("images/"+data['nombreImagen'], 'wb') # create a writable image and write the decoding result
				image_result.write(image_64_decode)
				if guardarProducto(data):
					return "Se aguardo exitosamente el producto"
				return json.dumps({'success':False}), 400, {'ContentType':'application/json'} 

			return render_template("agregarProducto.html")
	return render_template("index.html")
	
@app.route('/agregarCombo',methods=['GET','POST'])
def agregarCombo():
	if 'activo' in session:
		data=request.get_json()
		if data != None:
			print("NOMBRE: "+data['nombre'])
			print("json combo: ",data)
			if guardarCombo(data):
				return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 
			return json.dumps({'success',False}),400,{'ContentType':'application/json'}
		p=obtenerProductos()
		return render_template("agregarCombo.html",productos=p)
	return render_template("index.html")

@app.route('/obtenerPedidos',methods=['GET'])
def obtenerPedidos():
	if 'activo' in session:
		#p=obtenerPedidosBD();
		#return Response(response=json.dumps(p), status=200, mimetype="application/json")
		pedidosBD=obtenerPedidosBD()
		return render_template("administrador.html",pedidos=pedidosBD)
	return render_template("index.html")

@app.route('/out')
def salir():
	session.clear()
	return render_template('index.html')

@app.route('/cafeteria/registrarUsuario',methods=['POST'])
@cross_origin()
def registrarUsuario():
	correo=request.form['femail']
	password=request.form['passwordI']

	if(validarUsuario(correo)):
		return "Ya existe el usuario"
	numAbytes=os.urandom(16)
	clave= uuid.uuid4().hex 
	mensaje="De click en la siguiente direccion:"\
		+"http://10.42.0.1/cafeteria/login/confirm?data="+clave
	msg = Message("Confirmar registro al sistema de pedidos de la cafeteria",
                  sender="cafeteria@tuscomidas.com",
                  recipients=[correo])
	msg.body = mensaje
	#mail.send(msg)
	#enviarEmail("cafeteria",correo,mensaje,"Confirmar registro al sistema de pedidos de la cafeteria")
	usuario = Usuario()
	usuario.setCorreo(correo)
	usuario.setPassword(password)
	usuario.setClave(clave)
	usuario.setActivo(False)
	guardarUsuario(usuario)
	return "Se mando una clave a su correo para la confirmación"
	#return render_template('index.html', error=error)

@app.route('/cafeteria/login/confirm')
@cross_origin()
def confirmarRegistro():
	clave=request.args.get('data')
	print(clave)
	mensaje=validarRegistro(clave)

	return "<p>"+mensaje+"</p>"

@app.route('/cafeteria/login',methods=["POST"])
@cross_origin()
def login():
	usu=validarUsuario(request.form['txtuser'])
	passCifrada=sha256(request.form['txtpassword'].encode()).hexdigest()
	print("Verfica si es valido el usuario pass: "+passCifrada)
	print("pass2: "+str(usu))
	if usu is not None 	and usu == passCifrada:
		session['activo']=True
		print("Inicio sesion: "+usu)
		return Response(response='{"success":"True"}', status=200, mimetype="application/json")
	return Response(response='{"success":"False"}', status=400, mimetype="application/json")

@app.route('/cafeteria/obtenerProductosXML')
@cross_origin()
def obtenerProductosXML():
	productos=obtenerProductos()
	xmlProductos=etree.Element("productos")
	for producto in productos:
		xmlProducto=etree.Element("producto")
		xmlProducto.attrib['id']=str(producto['id'])
		xmlProducto.attrib['tipo']=producto['tipo']

		xmlProducto.append(etree.Element("nombre"))
		xmlProducto.append(etree.Element("descripcion"))
		xmlProducto.append(etree.Element("precio"))
		xmlProducto.append(etree.Element("imagen_url"))
		xmlProducto[0].text=producto['nombre']
		xmlProducto[1].text=producto['descripcion']
		xmlProducto[2].text=str(producto['precio'])
		xmlProducto[3].text=producto['url']

		xmlProductos.append(xmlProducto)

	cad = etree.tostring(xmlProductos, pretty_print=True)
	r = Response(response=cad, status=200, mimetype="application/xml")
	return r

@app.route('/cafeteria/obtenerCombosXML')
@cross_origin()
def obtenerCombosXML():
	combos=obtenerCombos()
	xmlCombos=etree.Element("combos")
	for combo in combos:
		xmlCombo=etree.Element("combo")
		xmlCombo.attrib['id']=str(combo['id'])

		xmlCombo.append(etree.Element("nombre"))
		xmlCombo.append(etree.Element("descripcion"))
		xmlCombo.append(etree.Element("precio"))

		xmlCombo[0].text=combo['nombre']
		xmlCombo[1].text=combo['descripcion']
		xmlCombo[2].text=str(combo['precio'])
		
		productos=combo['productos']
		xmlProductos=etree.Element("productos")
		for producto in productos:
			xmlProducto=etree.Element("producto")
			xmlProducto.attrib['tipo']=producto['tipo']

			xmlProducto.append(etree.Element("nombre"))
			xmlProducto.append(etree.Element("descripcion"))
			xmlProducto.append(etree.Element("imagen_url"))
			xmlProducto[0].text=producto['nombre']
			xmlProducto[1].text=producto['descripcion']
			xmlProducto[2].text=producto['url']
			
			xmlProductos.append(xmlProducto)

		xmlCombo.append(xmlProductos)
		xmlCombos.append(xmlCombo)

	cad = etree.tostring(xmlCombos, pretty_print=True)
	r = Response(response=cad, status=200, mimetype="application/xml")
	return r

@app.route('/cafeteria/hacerPedido',methods=["POST"])
@cross_origin()
def agregarPedido():
	print("Se ingreso al agregarPedido")
	data=request.get_json()
	print(data)
	exito=agregarOrden(data)
	if exito:
		print("estodo de pedido ",hayNuevoPedido)
		return "Se completo el pedido exitosamente"
	return "No se pudo completar el pedido"


@app.route('/cafeteria/logout')
@cross_origin()
def logout():
	session.clear()
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 

@app.route('/menus')
def comidas():
	f=open('templates/comidas.xml','r')
	documento = etree.parse('templates/comidas.xml')
	r = Response(response=f, status=200, mimetype="application/xml")
	return r

@app.route('/noticia')
def getNoticia():
	noticias=""#getNoticias("http://www.reforma.com/rss/universitarios.xml")
	i=randint(0,len(noticias)-1)
	cadena='<a href="'+noticias[i]['url']+'">'+noticias[i]['titulo']+'</a>'
	return cadena




@app.route('/app')
def getApp():
	return  send_from_directory("templates/", "PedidosCafeteria.zip", as_attachment=True) 
	#return "aplicacion"
#Si no se define el parametro host, flask solo sera visible desde localhost
# app.run(host='localhost')
if __name__ == '__main__': 
	app.run(host='0.0.0.0',port=8080)

#Para generar claves 
#import os
# print(os.urandom(16))
